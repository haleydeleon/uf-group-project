
# coding: utf-8

# In[ ]:

import csv #Import csv
from datetime import datetime
import jsonpickle
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')
import numpy as np
import os
import pandas as pd
import sys
import tweepy


# In[1]:

def hashtag_tweets(hashtag):
        """ Args:
            hashtag (str): hashtag whose tweets you want to grab   

        Returns:
            hashtag_tweets_chart: line graph depicting use of hashtag on a per hour basis over the course of a week
            hashtag_tweets.csv: csv file containing the timestamp of every tweet using the hashtag over 7 days and a count of total tweets for the hashtag

        Raises:
            TweepError: "some error : " + str(TweepError) """

        searchQuery = hashtag  # this is what we're searching for
        maxTweets = 10000000 # Some arbitrary large number
        tweetsPerQry = 100  # this is the max the API permits
        fname = 'hashtag_tweets.csv' #file to save tweet info

        csvFile = open('hashtag_tweets.csv', 'a') #open file where tweet info will be stored 
        csvwriter= csv.writer(csvFile, delimiter=' ') #automatically reads tweet data and parses
        csvwriter.writerow(['DateTime']) #only show the Timestamp for the tweets

        # If results from a specific ID onwards are reqd, set since_id to that ID.
        # else default to no lower limit, go as far back as API allows
        sinceId = None

        # If results only below a specific ID are, set max_id to that ID.
        # else default to no upper limit, start from the most recent tweet matching the search query.
        max_id = (-1)

        tweetCount = 0
        print("Downloading max {0} tweets".format(maxTweets)) #Graphical help to show progress
        with open(fname, 'w') as f:
            while tweetCount < maxTweets:
                try:
                    if (max_id <= 0):
                        if (not sinceId):
                            new_tweets = api.search(q=searchQuery, count=tweetsPerQry)
                        else:
                            new_tweets = api.search(q=searchQuery, count=tweetsPerQry,
                                                    since_id=sinceId)
                    else:
                        if (not sinceId):
                            new_tweets = api.search(q=searchQuery, count=tweetsPerQry,
                                                    max_id=str(max_id - 1))
                        else:
                            new_tweets = api.search(q=searchQuery, count=tweetsPerQry,
                                                    max_id=str(max_id - 1),
                                                    since_id=sinceId)
                    if not new_tweets:
                        print("No more tweets found")
                        break
                    for tweet in new_tweets:
                         csvwriter.writerow([tweet.created_at]) #recording the tweet info
                    tweetCount += len(new_tweets)
                    print("Downloaded {0} tweets".format(tweetCount))
                    max_id = new_tweets[-1].id
                except tweepy.TweepError as e:
                    # Just exit if any error
                    print("some error : " + str(e))
                    break

        print ("Downloaded {0} tweets, Saved to {1}".format(tweetCount, fname))

        csvFile.close()

        hashtag_tweets_df= pd.read_csv('../FinalProject/hashtag_tweets.csv') #read tweet info
        hashtag_tweets_df.insert(1, 'Tweet Count', range(len(hashtag_tweets_df))) #insert column to count tweets
        hashtag_tweets_df['DateTime'] = pd.to_datetime(hashtag_tweets_df['DateTime']) #making sure it recognizes the entries as timestamps

        ts = hashtag_tweets_df.set_index('DateTime')
        hashtag_tweets_df= ts.resample('H').count() #shows the count of number of entries per hour, will help with graphical processing

        hashtag_tweets_chart=hashtag_tweets_df.plot.line(legend=False) #plot the chart


# In[ ]:

def GoogleTrendsNSDxMT(kw_list):
    """ Args:
        kw_list ([str]): keywords used in Google Trends, 'National Sandwich Day, Me Too'   
        
    Returns:
        GoogleTrendsNSDxMT_chart: line graph depicting search of keywords over time
        
    Raises:
        ValueError: Opps, something went wrong. Please check your syntax and try again. """
    #search items
    kw_list = ["National Sandwich Day", "Me too"] 
    
    #load paramaters for data, not necessary for all GoogleTrends API uses
    pytrends.build_payload(kw_list, timeframe='now 7-d', geo='US') 
    
    #actual pulling of data 
    GoogleTrendsNSDxMT_df=pytrends.interest_over_time()
    
    #parsing dataframe to the relevant columns
    GoogleTrendsNSDxMT_df=GoogleTrendsNSDxMT_df[['National Sandwich Day', 'Me too']]
    
    #plot chart
    GoogleTrendsNSDxMT_chart= GoogleTrendsNSDxMT_df.plot.line()


# In[ ]:

def GoogleTrendsiPXxWSG7(kw_list):
    """ Args:
        kw_list ([str]): keywords used in Google Trends, 'IPhone X, World Series Game 7'   
        
    Returns:
        GoogleTrendsiPXxWSG7_chart: line graph depicting search of keywords over time
        
    Raises:
        ValueError: Opps, something went wrong. Please check your syntax and try again. """
        
    
    #search items
    kw_list= [ "iPhone X","World Series Game 7"]
    
    #load parameters for data pull, not necessary for all Google Trends API uses
    pytrends.build_payload(kw_list, timeframe='now 7-d', geo='US')
    
    #actual pulling of data
    GoogleTrendsiPXxWSG7_df=pytrends.interest_over_time()
    
    #parsing dataframe down to relevant columns
    GoogleTrendsiPXxWSG7_df=GoogleTrendsiPXxWSG7_df[['iPhone X', 'World Series Game 7']]
    
    #plot chart
    GoogleTrendsiPXxWSG7_chart= GoogleTrendsiPXxWSG7_df.plot.line()

