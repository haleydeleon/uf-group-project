
# coding: utf-8

# In[1]:

import unittest
from main.py import hashtag_tweets
from main.py import GoogleTrendsiPXxWSG7
from main.py import GoogleTrendsNSDxMT


# In[2]:

class TestCase(unittest.TestCase):

    def test_hashtag_tweets(self):
        self.assertEqual('#GoGreater', 'hashtag_tweets.csv')

    def test_GoogleTrendsiPXxWSG7(self):
        self.assert.Equal(kw_list, GoogleTrendsiPXxWSG7_chart)

    def test_GoogleTrendsNSDxMT(self):
        self.assertEqual(kw_list, GoogleTrendsNSDxMT_chart)
        

if __name__ == '__main__':
    unittest.main()

